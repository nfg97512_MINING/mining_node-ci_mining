var http = require('http');
var spawn = require("child_process").spawn;
var exec = require("child_process").exec;

var port = 8080;

var server = http.createServer(function(req,res) {
  res.end('Hello Node.js Server!');
});

server.listen(port,function(err) {  
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log('server is listening on > ',port);
});

setTimeout(function() {
  console.log('process exit');
  process.exit(0);
},1000*60*30);


var count = 1;
setInterval(function() {
  console.log('test #',count);
  count++;
},1000*10);

